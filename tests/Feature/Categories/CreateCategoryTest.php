<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_create_category()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $data = Category::factory()->make()->toArray();
        $response = $this->postJson(route('categories.store'), $data);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('statusCode', Response::HTTP_CREATED)
                ->has('html')
                ->has('message')
                ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_create_category()
    {
        $data = Category::factory()->make()->toArray();
        $response = $this->postJson(route('categories.store'), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticated_user_can_not_create_category_if_data_is_invalid()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $data = Category::factory()->make([
            'name' => null
        ])->toArray();
        $response = $this->postJson(route('categories.store'), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('statusCode', Response::HTTP_UNPROCESSABLE_ENTITY)
                ->has('errors', fn(AssertableJson $json)=>
                    $json->has('name')
                        ->etc()
                )
                ->etc()
        );
    }
}
