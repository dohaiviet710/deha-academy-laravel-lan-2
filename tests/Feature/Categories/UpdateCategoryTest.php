<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_category_if_category_exists()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $category = Category::factory()->create();
        $data = Category::factory()->make()->toArray();
        $response = $this->putJson(route('categories.update', $category->id), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('statusCode', Response::HTTP_OK)
                ->has('html')
                ->has('table')
                ->has('message')
                ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_category()
    {
        $category = Category::factory()->create();
        $data = Category::factory()->make()->toArray();
        $response = $this->putJson(route('categories.update', $category->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticated_user_can_not_update_category_if_category_not_exists()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $data = Category::factory()->make()->toArray();
        $response = $this->putJson(route('categories.update', -1), $data);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('statusCode', Response::HTTP_NOT_FOUND)
                ->has('errors')
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_update_category_if_data_is_invalid()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $category = Category::factory()->create();
        $data = Category::factory()->make([
            'name' => null
        ])->toArray();
        $response = $this->putJson(route('categories.update', $category->id), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('statusCode', Response::HTTP_UNPROCESSABLE_ENTITY)
                ->has('errors', fn(AssertableJson $json)=>
                    $json->has('name')
                        ->etc()
                    )
                ->etc()
        );
    }
}
