<?php

namespace Tests\Feature\Categories;

use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_list_category()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $response = $this->get(route('categories.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
    }

    /** @test */
    public function untauthenticated_user_can_not_get_list_category()
    {
        $response = $this->get(route('categories.index'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }
}
