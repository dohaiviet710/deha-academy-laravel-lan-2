<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_create_product()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $countRecord = Product::count();
        Storage::disk('image');
        $dataProduct = Product::factory()->make()->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->postJson(route('products.store'), $dataProduct);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('statusCode', Response::HTTP_CREATED)
            ->has('html')
            ->has('message')
            ->etc()
        );
        $this->assertDatabaseCount('products', $countRecord + 1);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_product()
    {
        Storage::disk('image');
        $dataProduct = Product::factory()->make()->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->postJson(route('products.store'), $dataProduct);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticated_user_can_not_create_product_if_data_invalid()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        Storage::disk('image');
        $dataProduct = Product::factory()->make([
            'name' => null
        ])->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->postJson(route('products.store'), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('statusCode', Response::HTTP_UNPROCESSABLE_ENTITY)
            ->has('errors', fn(AssertableJson $json)=>
            $json->has('name')
                ->etc()
            )
            ->etc()
        );
    }
}
