<?php

namespace Tests\Feature\Products;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_get_list_products()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
    }

    /** @test  */
    public function unauthenticated_user_can_not_get_list_products()
    {
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }
}
