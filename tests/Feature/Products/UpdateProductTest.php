<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_users_if_data_pass_validate()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        Storage::disk('image');
        $dataProduct = Product::factory()->make()->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->putJson(route('products.update', $product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_OK);
        $product = Product::find($product->id);
        $this->assertEquals($dataProduct['name'], $product->name);
    }

    /** @test  */
    public function authenticated_user_can_not_update_product_if_data_not_pass_validate()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        Storage::disk('image');
        $dataProduct = Product::factory()->make([
            'name' => null
        ])->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->putJson(route('products.update', $product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('name')
        )->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_update_product_if_product_not_exits()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        Storage::disk('image');
        $dataProduct = Product::factory()->make()->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->put(route('products.update', -1), $dataProduct);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('statusCode', Response::HTTP_NOT_FOUND)
            ->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_product()
    {
        $product = Product::factory()->create();
        Storage::disk('image');
        $dataProduct = Product::factory()->make()->toArray();
        $dataProduct['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->putJson(route('products.update', $product->id), $dataProduct);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }
}
