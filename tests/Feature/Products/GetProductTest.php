<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetProductTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_get_products()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $response = $this->getJson(route('products.show', $product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('statusCode')
            ->has('html')
            ->etc()
        );
    }

    /** @test  */
    public function authenticated_user_can_not_get_products_if_product_not_exits()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $response = $this->getJson(route('products.show', -1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('statusCode')
            ->has('errors')
            ->etc()
        );
    }
}
