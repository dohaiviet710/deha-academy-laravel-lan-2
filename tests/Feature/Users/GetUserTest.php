<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetUserTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_get_user()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $response = $this->get(route('users.show', $user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('statusCode')
            ->has('html')
            ->etc()
        );
    }

    /** @test  */
    public function authenticated_user_can_not_get_user()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $response = $this->get(route('users.show', -1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) => $json->has('statusCode')
            ->has('errors')
            ->etc()
        );
    }
}
