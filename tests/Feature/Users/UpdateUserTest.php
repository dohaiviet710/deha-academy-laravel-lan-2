<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_user_if_data_is_valid()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $user = User::factory()->create();
        Storage::disk('image');
        $dataUser = User::factory()->make()->toArray();
        $dataUser['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->putJson(route('users.update', $user->id), $dataUser);
        $response->assertStatus(Response::HTTP_OK);
        $user = User::find($user->id);
        $this->assertEquals($dataUser['name'], $user->name);
    }

    /** @test  */
    public function authenticated_user_can_not_update_user_if_data_not_pass_validate()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $user = User::factory()->create();
        Storage::disk('image');
        $dataUser = User::factory()->make([
            'name' => null
        ])->toArray();
        $dataUser['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->putJson(route('users.update', $user->id), $dataUser);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('name')
        )->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_user_not_exits()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        Storage::disk('image');
        $dataUser = User::factory()->make()->toArray();
        $dataUser['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->putJson(route('users.update', -1), $dataUser);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('statusCode', Response::HTTP_NOT_FOUND)
            ->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_user()
    {
        $user = User::factory()->create();
        Storage::disk('image');
        $dataUser = User::factory()->make()->toArray();
        $dataUser['image'] = UploadedFile::fake()->image(time() . '.png');
        $response = $this->putJson(route('users.update', $user->id), $dataUser);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }
}
