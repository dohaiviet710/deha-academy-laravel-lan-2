<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_delete_user_if_user_exist()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $product = User::factory()->create();
        $this->actingAs($user);
        $countRecord = User::count();
        $response = $this->deleteJson(route('users.delete', $product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('statusCode', Response::HTTP_OK)
            ->has('table')
            ->etc()
        );
        $this->assertDatabaseCount('users', $countRecord - 1);
    }

    /** @test  */
    public function authenticated_user_can_not_delete_use_if_user_not_exist()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $data = User::factory()->make()->toArray();
        $response = $this->putJson(route('users.update', -1), $data);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('statusCode', Response::HTTP_NOT_FOUND)
            ->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_user()
    {
        $product = User::factory()->create();
        $response = $this->deleteJson(route('users.delete', $product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }
}
