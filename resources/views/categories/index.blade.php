@extends('layouts.main')
@section('contents')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Categories Tables</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card card-outline">
                            <div class="card-header">
                                <div class="card-tools float-left">
                                    <form id="form-search" action="{{ route('categories.search') }}"
                                          class="input-group input-group-sm" style="width: 150%;">
                                        <input autocomplete="off"
                                               id="name-txt" type="text" name="name"
                                               class="form-control" placeholder="Search category">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                @can('create', \App\Models\Category::class)
                                    <div class="card-tools float-right">
                                        <div class="input-group input-group-sm" style="width: 150%;">
                                            <a href="{{ route('categories.create') }}" class="btn btn-sm btn-success btn-open-modal"
                                               data-toggle="modal">
                                                <i class="fas fa-plus-square mr-2"></i>
                                                <span>Create</span>
                                            </a>
                                        </div>
                                    </div>
                                @endcan
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <div id="table">
                                    @include('categories.paginate')
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <div id="modal">

    </div>
@endsection
