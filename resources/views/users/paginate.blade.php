<table class="table table-hover text-nowrap">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Image</th>
        <th>Gender</th>
        <th>Email</th>
        <th>Status</th>
        <th>Action(s)</th>
    </tr>
    </thead>
    <tbody>
    @if($users->isNotEmpty())
        @foreach($users as $user)
            <tr>
                <td>
                    {{ $user->id }}
                </td>
                <td>
                    {{ $user->name }}
                </td>
                <td>
                    <img src="{{ getImage($user->avatar) }}" alt=""
                         class="img-thumbnail img-circle" width="105px">
                </td>
                <td>
                    {{ getGenderUser($user->gender) }}
                </td>
                <td>
                    {{ $user->email }}
                </td>
                <td>
                    {!! getStatus($user->activated) !!}
                </td>
                <td>
                    @can('view', $user)
                        <a href="{{ route('users.show', $user->id) }}"
                           class="btn btn-sm btn-info btn-open-modal" data-toggle="modal">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endcan
                    @can('update', $user)
                        <a href="{{ route('users.edit', $user->id) }}"
                           class="btn btn-sm btn-secondary btn-open-modal" data-toggle="modal">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endcan
                    @can('delete', $user)
                        <a href="{{ route('users.delete', $user->id) }}"
                           class="btn btn-sm btn-danger btn-delete-item">
                            <i class="fas fa-trash"></i>
                        </a>
                    @endcan
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="7" class="text-center">
                <span style="color: red">No data found.</span>
            </td>
        </tr>
    @endif
    </tbody>
</table>
<div class="mt-3 mr-2 ml-2">
    {{ $users->links() }}
</div>
