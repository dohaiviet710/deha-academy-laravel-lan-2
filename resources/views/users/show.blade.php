<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail product</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">

                        <!-- Profile Image -->
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle"
                                         src="{{ getImage($user->avatar) }}"
                                         alt="User profile picture">
                                </div>

                                <h3 class="profile-username text-center">
                                    {{ $user->name }}
                                </h3>

                                <p class="text-muted text-center">
                                    {{ $user->role->name ?? 'Member' }}
                                </p>

                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Email</b>
                                        <a class="float-right">
                                            {{ $user->email }}
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Phone</b>
                                        <a class="float-right">
                                            {{ convertPhone($user->phone) }}
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Status</b>
                                        <a class="float-right">
                                            {!! getStatus($user->activated) !!}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                        <!-- About Me Box -->

                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-9">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">About Me</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <strong><i class="fas fa-venus-mars mr-1"></i> Gender</strong>

                                <p class="text-muted">
                                    {{ getGenderUser($user->gender) }}
                                </p>

                                <hr>
                                <strong><i class="fas fa-book mr-1"></i> Birthday</strong>

                                <p class="text-muted">
                                   {{ convertDate($user->birthday) }}
                                </p>

                                <hr>

                                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                                <p class="text-muted">
                                    {!! $user->address !!}
                                </p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
