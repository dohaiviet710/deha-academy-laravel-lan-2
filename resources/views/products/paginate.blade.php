<table class="table table-hover text-nowrap">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Image</th>
        <th>Price</th>
        <th>Stock</th>
        <th>Status</th>
        <th>Action(s)</th>
    </tr>
    </thead>
    <tbody>
    @if($products->isNotEmpty())
        @foreach($products as $product)
            <tr>
                <td>
                    {{ $product->id }}
                </td>
                <td>
                    {{ $product->name }}
                </td>
                <td>
                    <img src="{{ getImage($product->image) }}" alt=""
                         class="img-thumbnail img-circle" width="105px">
                </td>
                <td>
                    {{ number_format($product->price) }}
                </td>
                <td>
                    {{ number_format($product->stock) }}
                </td>
                <td>
                    {!! getStatus($product->activated) !!}
                </td>
                <td>
                    @can('view', $product)
                        <a href="{{ route('products.show', $product->id) }}"
                           class="btn btn-sm btn-info btn-open-modal" data-toggle="modal">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endcan
                    @can('update', $product)
                        <a href="{{ route('products.edit', $product->id) }}" class="btn btn-sm btn-secondary btn-open-modal"
                            data-toggle="modal">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endcan

                    @can('delete', $product)
                        <a href="{{ route('products.delete', $product->id) }}" class="btn btn-sm btn-danger btn-delete-item">
                            <i class="fas fa-trash"></i>
                        </a>
                    @endcan

                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="7" class="text-center">
                <span style="color: red">No data found.</span>
            </td>
        </tr>
    @endif
    </tbody>
</table>
<div class="mt-3 mr-2 ml-2">
    {{ $products->links() }}
</div>
