@extends('layouts.main')
@section('contents')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Products Tables</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card card-outline">
                            <div class="card-header">
                                <div class="card-tools float-left">
                                    <div class="input-group input-group-sm" style="width: 200%;">
                                        <form id="form-search" action="{{ route('products.search') }}"
                                              style="width: 100%;" method="POST">
                                            @csrf
                                            @method('POST')
                                            <div class="d-flex flex-row mb-3">
                                                <select name="category" id="category"
                                                        class="form-control form-control-sm mr-3">
                                                    <option value="">------>Select Category<------</option>
                                                    @foreach($categories as $category)
                                                        <option
                                                            value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                                <input name="name" id="name-txt"
                                                       class="form-control form-control-sm" type="text"
                                                       placeholder="Name product">
                                            </div>
                                            <div class="d-flex flex-row mb-3">
                                                <input name="price_form" id="price-from"
                                                       class="form-control form-control-sm mr-3" type="number" min="0"
                                                       placeholder="Price to">
                                                <input name="price_to" id="price-to"
                                                       class="form-control form-control-sm" type="number" min="0"
                                                       placeholder="Price from">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                @can('create', \App\Models\Product::class)
                                    <div class="card-tools float-right">
                                        <div class="input-group input-group-sm" style="width: 150%;">
                                            <a href="{{ route('products.create') }}"
                                               class="btn btn-sm btn-success btn-open-modal" data-toggle="modal">
                                                <i class="fas fa-plus-square mr-2"></i> Create
                                            </a>
                                        </div>
                                    </div>
                                @endcan
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <div id="table">
                                    @include('products.paginate')
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <div id="modal">

    </div>
@endsection
