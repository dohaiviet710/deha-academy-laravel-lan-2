<?php
namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait HandleImage {

    const WIDTH_IMG = 150;
    const HEIGHT_IMG = 150;
    const QUALITY_IMG = 80;

    public string $path;
    public string $disk = 'image';

    public function saveIMG($image): string
    {
        $typeIMG = $image->getClientOriginalExtension();
        $image = Image::make($image)->resize(self::WIDTH_IMG,self::HEIGHT_IMG)
            ->encode($typeIMG, self::QUALITY_IMG);
        $name = time() . '.' . $typeIMG;
        $fullPath = $this->path . $name;
        Storage::disk($this->disk)->put($fullPath, $image);
        return $name;
    }

    public function updateIMG($image, $name): string
    {
        if ($this->imageExists($name))
        {
            $result = $this->deleteImage($name);
        }
        return $this->saveIMG($image);
    }

    public function imageExists($name): bool|string
    {
        $name = str_replace('images/', '', $name);
        if (Storage::disk($this->disk)->exists($name) && !empty($name))
        {
            return $name;
        }
        return false;
    }

    public function deleteImage($name): bool
    {
        $name = str_replace('images/', '', $name);
        return Storage::disk($this->disk)->delete($name);
    }

    /**
     * @param  string  $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path . '/';
    }

    /**
     * @param  string  $disk
     */
    public function setDisk(string $disk): void
    {
        $this->disk = $disk;
    }
}
