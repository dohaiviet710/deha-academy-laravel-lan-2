<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    const PATH_IMAGE = 'images/products/';
    protected $table = 'products';
    protected $fillable = [
        'name',
        'slug',
        'image',
        'price',
        'stock',
        'descriptions',
        'activated'
    ];

    protected function image(): Attribute
    {
        return Attribute::get(function ($image){
            if (!isset($image)) {
                return null;
            }
            if (str_contains($image, 'http')) {
                return $image;
            }
            return self::PATH_IMAGE . $image;
        }
        );
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'categories_products',
            'product_id', 'category_id');
    }

    public function scopeCategory($query, $id)
    {
        return $query->when($id, function ($query) use ($id){
            $query->whereHas('categories', function ($query) use ($id) {
                $query->where('category_id', $id);
            });
        });
    }

    public function scopeName($query, $name)
    {
        return $query->when($name, function ($query) use ($name){
            $query->where('name', 'like', '%' . $name . '%');
        });
    }

    public function scopePriceFrom($query, $price)
    {
        return $query->when($price, function ($query) use ($price){
            $query->where('price', '>=', $price);
        });
    }

    public function scopePriceTo($query, $price)
    {
        return $query->when($price, function ($query) use ($price){
            $query->where('price', '<=', $price);
        });
    }
}
