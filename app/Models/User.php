<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    const EMAIL_ADMIN = 'admin@deha-soft.com';
    const PATH_AVATAR = 'images/users/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'gender',
        'birthday',
        'avatar',
        'email',
        'phone',
        'password',
        'address',
        'role_id',
        'activated',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function avatar(): Attribute
    {
        return Attribute::get(function ($avatar){
            if (!isset($avatar)) {
                return null;
            }
            if (str_contains($avatar, 'http')) {
                return $avatar;
            }
            return self::PATH_AVATAR . $avatar;
        }
        );
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function scopeUserName($query, $value)
    {
        return $query->where('name', 'like', '%' . $value . '%');
    }

    public function scopeUserEmail($query, $value)
    {
        return $query->orWhere('email', 'like', '%' . $value . '%');
    }

    public function isAdmin(): bool
    {
        if ($this->email === self::EMAIL_ADMIN){
            return true;
        }
        return false;
    }

    public function getPermissions()
    {
        if (!empty($this->role))
        {
            return $this->role->permissions->pluck('route_name')->toArray();
        }
        return [];
    }

    public function checkRole($permission)
    {
        return in_array($permission, $this->getPermissions());
    }
}
