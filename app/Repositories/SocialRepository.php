<?php
namespace App\Repositories;

use App\Models\Social;

class SocialRepository extends BaseRepository
{
    public function model(): string
    {
        return Social::class;
    }

    public function getSocial($data)
    {
        return $this->model->provider($data['provider'])
                    ->providerId($data['provider_id'])->first();
    }
}
