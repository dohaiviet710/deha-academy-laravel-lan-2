<?php
namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    public function model(): string
    {
        return User::class;
    }

    public function search($name)
    {
        return $this->model->userName($name)->userEmail($name);
    }

    public function findEmail($email)
    {
        return $this->model->userEmail($email)->first();
    }
}
