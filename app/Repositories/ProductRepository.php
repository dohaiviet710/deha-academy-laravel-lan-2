<?php
namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    public function model(): string
    {
        return Product::class;
    }

    public function search($data)
    {
        return $this->model->name($data['name'] ?? null)
            ->category($data['category'] ?? null)
            ->priceFrom($data['price_form'] ?? null)
            ->priceTo($data['price_to'] ?? null)
            ->latest('id');
    }
}
