<?php
if (!function_exists('getAvatarUser'))
{
    function getImage($url): string
    {
        if (empty($url)){
            return asset('assets/dist/img/death-cat-circle.jpg');
        }
        if (strpos('http', $url)){
            return $url;
        }
        return asset($url);
    }
}

if (!function_exists('getGenderUser'))
{
    function getGenderUser($gender): string
    {
        if (is_null($gender)){
            return '?';
        }
        if ($gender == 1){
            return 'Man';
        }
        return 'Women';
    }
}

if (!function_exists('convertDate')){
    function convertDate($date, $format = 'd-m-Y'): string
    {
        if (isset($date)){
            return date($format, strtotime($date));
        }
        return '?';
    }
}

if (!function_exists('convertPhone'))
{
    function convertPhone($phone): string
    {
        if(!empty($phone) && preg_match( '/^(\d{4})(\d{3})(\d{3})$/', $phone,  $matches ))
        {
            return $matches[1] . '-' .$matches[2] . '-' . $matches[3];
        }

        return '?';
    }
}

if (!function_exists('convertDateMark'))
{
    function convertDateMark($date): bool|string
    {
        if (isset($date) && !str_contains($date, '-')){
            $arr = explode('/', $date);
            return $arr[2] . '-' . $arr[1] . '-' . $arr[0];
        }
        return $date;
    }
}

if (!function_exists('getStatus'))
{
    function getStatus($status): string
    {
        if ($status){
            return '<span class="badge badge-success">Activated</span>';
        }
        return '<span class="badge badge-secondary">Inactivated</span>';
    }
}
