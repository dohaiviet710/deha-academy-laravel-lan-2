<?php
namespace App\Services;

use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Traits\HandleImage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductService
{
    use HandleImage;
    public ProductRepository $productRepository;
    public CategoryRepository $categoryRepository;
    public string $pathProduct = 'products';

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->setPath($this->pathProduct);
    }

    public function index()
    {
        return $this->productRepository->latest('id')->paginate(10)
            ->withPath(route('products.index'));
    }

    public function find($id)
    {
        return $this->productRepository->find($id);
    }

    public function store(Request $request)
    {
        $request->merge([
            'slug' => Str::slug($request->input('name'))
        ]);
        $dataProduct = $request->except('categories');
        if ($request->hasFile('image')){
            $name = $this->saveIMG($request->image);
            $dataProduct['image'] = $name;
        }
        $categories = $request->categories;
        $product = $this->productRepository->create($dataProduct);
        $product->categories()->attach($categories);
        return $product;
    }

    public function update(Request $request, $id)
    {
        $request->merge([
            'slug' => Str::slug($request->input('name'))
        ]);
        if (!$request->filled('activated')){
            $request->merge([
                'activated' => 0
            ]);
        }
        $dataProduct = $request->except('categories');
        $product = $this->productRepository->find($id);
        if ($request->hasFile('image')){
            $name = $this->updateIMG($request->image, $product->image);
            $dataProduct['image'] = $name;
        }
        $categories = $request->categories;
        $product = $this->productRepository->update($dataProduct, $id);
        $product->categories()->sync($categories);
        return $product;
    }

    public function delete($id)
    {
        $product = $this->productRepository->find($id);
        if ($this->imageExists($product->image)){
            $this->deleteImage($product->image);
        }
        return $this->productRepository->delete($id);
    }

    public function search($request)
    {
        return $this->productRepository->search($request->all())->paginate(10)
                ->withPath(route('products.index'));
    }
}
