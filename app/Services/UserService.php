<?php
namespace App\Services;

use App\Repositories\RoleRepository;
use App\Repositories\SocialRepository;
use App\Repositories\UserRepository;
use App\Traits\HandleImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class UserService
{
    use HandleImage;
    public string $pathUser = 'users';
    public UserRepository $userRepository;
    public SocialRepository $socialRepository;
    public RoleRepository $roleRepository;

    public function __construct(UserRepository $userRepository,
                                SocialRepository $socialRepository,
                                RoleRepository $roleRepository,
                                )
    {
        $this->userRepository = $userRepository;
        $this->socialRepository = $socialRepository;
        $this->roleRepository = $roleRepository;
        $this->setPath($this->pathUser);
    }

    public function login(Request $request)
    {
        return Auth::attempt($request->only('email', 'password'), $request->remember);
    }

    public function register(Request $request)
    {
        $data = $request->merge([
            'password' => bcrypt($request->input('password')),
            'activated' => 1
        ]);
        return $this->userRepository->create($data->except('confirm_password'));
    }

    public function socialCallback($provider)
    {
        try {
            $userSocial = Socialite::driver($provider)->user();
            $infoSocial = [
                'provider' => $provider,
                'provider_id' => $userSocial->getId()
            ];
            $social = $this->socialRepository->getSocial($infoSocial);
            $user = $this->userRepository->findEmail($userSocial->getEmail());
            if (empty($social) && empty($user)){
                $dataUser = [
                    'name' => $userSocial->name ?? $userSocial->getNickname(),
                    'email' => $userSocial->email,
                    'avatar' => $userSocial->avatar ?? null,
                    'activated' => 1
                ];
                $userNew = $this->userRepository->create($dataUser);

                $dataSocial = [
                    'provider' => $provider,
                    'provider_id' => $userSocial->getId(),
                    'user_id' => $userNew->id
                ];
                $socialLogin = $this->socialRepository->create($dataSocial);

                return $userNew;
            }

            if (empty($social)){
                $dataSocial = [
                    'provider' => $provider,
                    'provider_id' => $userSocial->getId(),
                    'user_uuid' => $user->id
                ];
                $socialLogin = $this->socialRepository->create($dataSocial);
            }

            return $user;

        }catch (\Exception $exception){
            return $exception->getMessage();
        };
    }

    public function index()
    {
        return $this->userRepository->latest('id')->paginate(10)
            ->withPath(route('users.index'));
    }

    public function show($id)
    {
        return $this->userRepository->find($id);
    }

    public function find($id)
    {
        return $this->userRepository->find($id);
    }

    public function update(Request $request, $id)
    {
        $request->merge([
            'role_id' => $request->input('role'),
        ]);
        if ($request->filled('password')){
            $request->merge([
                'password' => bcrypt($request->input('password'))
            ]);
        }
        if (!$request->filled('activated')){
            $request->merge([
                'activated' => 0
            ]);
        }
        $dataUser = $request->all();
        $user = $this->userRepository->find($id);
        if ($request->hasFile('avatar')){
            $nameAvatar = $this->updateIMG($request->avatar, $user->avatar);
            $dataUser['avatar'] = $nameAvatar;
        }
        return $this->userRepository->update($dataUser, $id);
    }

    public function delete($id)
    {
        return $this->userRepository->delete($id);
    }

    public function search($request)
    {
        return $this->userRepository->search($request->name)
                ->latest('id')->paginate(10)->withPath(route('users.index'));
    }
}
