<?php
namespace App\Services;


use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RoleService
{
    public RoleRepository $roleRepository;
    public PermissionRepository $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function index()
    {
        return $this->roleRepository->latest('id')->paginate(10)
            ->withPath(route('roles.index'));
    }

    public function create()
    {
        return $this->permissionRepository->latest('id')->get();
    }

    public function store(Request $request)
    {
        $request->merge([
            'slug' => Str::slug($request->input('name'))
        ]);
        $role = $this->roleRepository->create($request->except('permissions'));
        $role->permissions()->attach($request->permissions);
        return $role;
    }

    public function find($id)
    {
        return $this->roleRepository->find($id);
    }

    public function update(Request $request, $id)
    {
        $request->merge([
            'slug' => Str::slug($request->input('name'))
        ]);
        if (!$request->filled('activated')){
            $request->merge([
                'activated' => 0
            ]);
        };
        $role = $this->roleRepository->find($id);
        $product = $this->roleRepository->update($request->except('permissions'), $id);
        $role->permissions()->sync($request->permissions);
        return $product;
    }

    public function delete($id)
    {
        return $this->roleRepository->delete($id);
    }
}
