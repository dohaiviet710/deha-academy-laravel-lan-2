<?php
namespace App\Services;

use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class CategoryService
{
    public CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        return $this->categoryRepository->latest('id')->paginate(10)
                    ->withPath(route('categories.index'));
    }

    public function create()
    {
        return $this->categoryRepository->latest('id')->get();
    }

    public function store(Request $request)
    {
        $request->merge([
            'parent_id' => $request->input('category'),
            'slug' => Str::slug($request->input('name')),
        ]);

        return $this->categoryRepository->create($request->except('category'));
    }

    public function find($id)
    {
        return $this->categoryRepository->find($id);
    }

    public function update(Request $request, $id)
    {
        $request->merge([
            'parent_id' => $request->input('category'),
            'slug' => Str::slug($request->input('name')),
        ]);
        if (!$request->filled('activated')){
            $request->merge([
                'activated' => 0
            ]);
        };
        return $this->categoryRepository->update($request->except('category'), $id);
    }

    public function delete($id)
    {
        return $this->categoryRepository->delete($id);
    }

    public function search($request)
    {
        return $this->categoryRepository->search($request->name)
                ->paginate(10)->withPath(route('categories.index'));
    }
}
