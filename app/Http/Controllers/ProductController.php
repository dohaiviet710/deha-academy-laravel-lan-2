<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products\StoreRequest;
use App\Http\Requests\Products\UpdateRequest;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    protected ProductService $productService;
    protected CategoryService $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = $this->productService->index();
        $categories = $this->categoryService->index();
        return view('products.index', compact(['products', 'categories']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = $this->categoryService->index();
        return $this->renderModal('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $product = $this->productService->store($request);
        $products = $this->productService->index();
        return response()->json([
            'statusCode' => Response::HTTP_CREATED,
            'html' => view('products.show', compact('product'))->render(),
            'table' => view('products.paginate', compact('products'))->render(),
            'message' => 'The product has been created successfully.',
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = $this->productService->find($id);
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'html' => view('products.show', compact('product'))->render(),
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $categories = $this->categoryService->categoryRepository
                            ->latest('id')->get();
        $product = $this->productService->find($id);
        return $this->renderModal('products.update', compact(['categories', 'product']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id)
    {
        $product = $this->productService->update($request, $id);
        $products = $this->productService->index();
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'html' => view('products.show', compact('product'))->render(),
            'table' => view('products.paginate', compact('products'))->render(),
            'message' => 'The product has been updated successfully.',
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product = $this->productService->delete($id);
        $products = $this->productService->index();
        return $this->renderTable($products);
    }

    public function search(Request $request)
    {
        $products = $this->productService->search($request);
        return $this->renderTable($products);
    }

    public function renderTable($products)
    {
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'table' => view('products.paginate', compact('products'))->render(),
        ], Response::HTTP_OK);
    }

    public function renderModal($view, $data)
    {
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'html' => view($view, $data)->render(),
        ], Response::HTTP_OK);
    }
}
