<?php

namespace App\Http\Controllers;

use App\Http\Requests\Roles\StoreRequest;
use App\Http\Requests\Roles\UpdateRequest;
use App\Repositories\RoleRepository;
use App\Services\RoleService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RoleController extends Controller
{
    protected RoleService $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $roles = $this->roleService->index();
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $permissions = $this->roleService->create();
        return $this->renderModal('roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $role = $this->roleService->store($request);
        $roles = $this->roleService->index();
        return response()->json([
            'statusCode' => Response::HTTP_CREATED,
            'html' => view('roles.show', compact('role'))->render(),
            'table' => view('roles.paginate', compact('roles'))->render(),
            'message' => 'The role has been created successfully.',
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $role = $this->roleService->find($id);
        $permissions = $this->roleService->permissionRepository->latest('id')->get();
        return $this->renderModal('roles.update', compact(['role', 'permissions']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id)
    {
        $role = $this->roleService->update($request, $id);
        $roles = $this->roleService->index();
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'html' => view('roles.show', compact('role'))->render(),
            'table' => view('roles.paginate', compact('roles'))->render(),
            'message' => 'The role has been updated successfully.',
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $role = $this->roleService->delete($id);
        $roles = $this->roleService->index();
        return $this->renderTable($roles);
    }

    public function renderTable($roles)
    {
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'table' => view('roles.paginate', compact('roles'))->render(),
        ], Response::HTTP_OK);
    }

    public function renderModal($view, $data)
    {
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'html' => view($view, $data)->render(),
        ], Response::HTTP_OK);
    }
}
