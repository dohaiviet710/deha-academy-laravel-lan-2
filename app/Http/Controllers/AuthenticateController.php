<?php

namespace App\Http\Controllers;

use App\Http\Requests\Authenticate\LoginRequest;
use App\Http\Requests\Authenticate\RegisterRequest;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class AuthenticateController extends Controller
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function viewLogin()
    {
        if (Auth::check()){
            return redirect()->route('welcome');
        }
        return view('auths.login');
    }

    public function login(LoginRequest $request)
    {
        $login = $this->userService->login($request);
        if ($login){
            return redirect()->route('welcome')->withErrors([
                'success' => 'Logged in successfully.',
            ]);
        }
        return redirect()->route('auth.login')->withErrors([
            'errorLogin' => 'The email or password you entered is incorrect.',
        ]);
    }

    public function viewRegister()
    {
        return view('auths.register');
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->userService->register($request);
        if ($user){
            Auth::loginUsingId($user->id);
            return redirect()->route('welcome')->withErrors([
                'success' => 'Logged in successfully.',
            ]);
        }
        return redirect()->back()->withErrors([
            'errorRegister' => 'Failed to register an account.',
        ]);
    }

    public function socialRedirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function socialCallback($provider)
    {
        $user = $this->userService->socialCallback($provider);
        if ($user){
            Auth::loginUsingId($user->id);
            return redirect()->route('welcome')->withErrors([
                'success' => 'Logged in successfully.',
            ]);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('auth.login')->withErrors([
            'success' => 'The account has been logged out.',
        ]);
    }
}
