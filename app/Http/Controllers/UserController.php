<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\UpdateRequest;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public UserService $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = $this->userService->index();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = $this->userService->show($id);
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'html' => view('users.show', compact('user'))->render(),
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = $this->userService->find($id);
        $roles = $this->userService->roleRepository->latest('id')->get();
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'html' => view('users.update', compact(['user', 'roles']))->render(),
        ], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id)
    {
        $user = $this->userService->update($request, $id);
        $users = $this->userService->index();
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'html' => view('users.show', compact('user'))->render(),
            'table' => view('users.paginate', compact('users'))->render(),
            'message' => 'The user has been updated successfully.',
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = $this->userService->delete($id);
        $users = $this->userService->index();
        return $this->renderTable($users);
    }

    public function search(Request $request)
    {
        $users = $this->userService->search($request);
        return $this->renderTable($users);
    }

    public function renderTable($users)
    {
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'table' => view('users.paginate', compact('users'))->render(),
        ], Response::HTTP_OK);
    }
}
