<?php

namespace App\Http\Controllers;

use App\Http\Requests\Categories\StoreRequest;
use App\Http\Requests\Categories\UpdateRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    protected CategoryService $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = $this->categoryService->index();
        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        $categories = $this->categoryService->create();
        return $this->renderModal('categories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): Response
    {
        $category = $this->categoryService->store($request);
        $categories = $this->categoryService->index();
        return response()->json([
            'statusCode' => Response::HTTP_CREATED,
            'html' => view('categories.show', compact('category'))->render(),
            'table' => view('categories.paginate', compact('categories'))->render(),
            'message' => 'The category has been created successfully.',
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show($category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $category = $this->categoryService->find($id);
        $categories = $this->categoryService->categoryRepository->latest('id')
                            ->where('id', "<>", $id)->get();
        return $this->renderModal('categories.update', compact(['categories', 'category']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, $id)
    {
        $category = $this->categoryService->update($request, $id);
        $categories = $this->categoryService->index();
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'html' => view('categories.show', compact('category'))->render(),
            'table' => view('categories.paginate', compact('categories'))->render(),
            'message' => 'The category has been updated successfully.',
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $category = $this->categoryService->delete($id);
        $categories = $this->categoryService->index();
        return $this->renderTable($categories);
    }

    public function search(Request $request)
    {
        $categories = $this->categoryService->search($request);
        return $this->renderTable($categories);
    }

    public function renderTable($categories)
    {
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'table' => view('categories.paginate', compact('categories'))->render(),
        ], Response::HTTP_OK);
    }

    public function renderModal($view, $data)
    {
        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'html' => view($view, $data)->render(),
        ], Response::HTTP_OK);
    }
}
