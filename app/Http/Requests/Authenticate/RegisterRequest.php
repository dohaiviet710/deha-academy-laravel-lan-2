<?php

namespace App\Http\Requests\Authenticate;

use App\Rules\EmailRule;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|min:3|max:150',
            'email' => ['required', 'email', new EmailRule(), 'unique:users,email'],
            'password' => 'required|min:3|max:100',
            'confirm_password' => 'required|same:password'
        ];
    }
}
