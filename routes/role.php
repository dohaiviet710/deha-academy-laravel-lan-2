<?php

use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

Route::middleware('checkActiveUser')->group(function (){
    Route::controller(RoleController::class)->group(function (){
        Route::prefix('roles')->group(function (){
            Route::as('roles.')->group(function (){
                Route::get('/', 'index')->name('index')
                    ->middleware('permission:roles.index');

                Route::get('/create', 'create')->name('create')
                    ->middleware('permission:roles.update');

                Route::post('/store', 'store')->name('store')
                    ->middleware('permission:roles.create');

                Route::get('edit/{id}', 'edit')->name('edit')
                    ->middleware('permission:roles.update');

                Route::put('update/{id}', 'update')->name('update')
                    ->middleware('permission:roles.update');

                Route::delete('delete/{id}', 'destroy')->name('delete')
                    ->middleware('permission:roles.delete');
            });
        });
    });
});
