<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

Route::middleware('checkActiveUser')->group(function () {
    Route::controller(CategoryController::class)->group(function () {
        Route::prefix('categories')->group(function () {
            Route::as('categories.')->group(function () {
                Route::get('/', 'index')->name('index')
                    ->middleware('permission:categories.index');

                Route::get('/create', 'create')->name('create')
                    ->middleware('permission:categories.create');

                Route::post('/store', 'store')->name('store')
                    ->middleware('permission:categories.create');

                Route::get('edit/{id}', 'edit')->name('edit')
                    ->middleware('permission:categories.update');

                Route::put('update/{id}', 'update')->name('update')
                    ->middleware('permission:categories.update');

                Route::delete('delete/{id}', 'destroy')->name('delete')
                    ->middleware('permission:categories.delete');

                Route::post('search', 'search')->name('search')
                    ->middleware('permission:categories.index');
            });
        });
    });
});

