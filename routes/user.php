<?php
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::middleware('checkActiveUser')->group(function (){
    Route::controller(UserController::class)->group(function (){
        Route::prefix('users')->group(function (){
            Route::as('users.')->group(function (){
                Route::get('/', 'index')->name('index')
                    ->middleware('permission:users.index');

                Route::get('/show/{id}', 'show')->name('show')
                    ->middleware('permission:users.show');

                Route::get('edit/{id}', 'edit')->name('edit')
                    ->middleware('permission:users.update');

                Route::put('update/{id}', 'update')->name('update')
                    ->middleware('permission:users.update');

                Route::delete('delete/{id}', 'destroy')->name('delete')
                    ->middleware('permission:users.delete');

                Route::post('search', 'search')->name('search');
            });
        });
    });
});
